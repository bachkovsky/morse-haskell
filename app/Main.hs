module Main where

import Control.Monad (forever, when)
import Data.List (intercalate)
import Data.Traversable (traverse)
import Morse (morseToString, stringToMorse)
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess)
import System.IO (hGetLine, hIsEOF, isEOF, stdin)

main :: IO ()
main = do
  mode <- getArgs
  case mode of
    [arg] ->
      case arg of
        "from" -> convertFromMorse
        "to" -> convertToMorse
        _ -> argError
    _ -> argError
  where
    argError = do
      putStrLn "Please specify the first argument as being 'from' or 'to' morse, such as: morse to"
      exitFailure

convertToMorse :: IO ()
convertToMorse = convert stringToMorse (putStrLn . unwords)

convertFromMorse :: IO ()
convertFromMorse = convert morseToString putStrLn

convert :: (String -> Maybe a) -> (a -> IO ()) -> IO ()
convert convertInput onSuccess =
  forever $ do
    weAreDone <- isEOF
    when weAreDone exitSuccess
    line <- getLine
    convertLine line
  where
    convertLine line =
      case convertInput line of
        (Just value) -> onSuccess value
        Nothing -> do
          putStrLn $ "ERROR: " ++ line
          exitFailure